const argv = require('yargs')
  .command('crear', 'Crear una Tarea', {
    tarea: {
      demand: true,
      alias: 't',
      desc: 'Nombre de la tarea a realizar'
    }
  })
  .command('listar', 'Lista todas las tareas', {})
  .command('borrar', 'Elimina una tarea indicando el Indice', {
    indice: {
      demand: true,
      alias: 'i',
      desc: 'Indice de la tarea a Eliminar'
    }
  })
  .command('terminado', 'Completa la tarea', {
    indice: {
      demand: true,
      alias: 'i',
      desc: "Indice de la tarea a marcar como Completado"
    }
  })
  .help()
  .argv;

module.exports = {
  argv
}
