const { guardar, cargaDB } = require('../models/tareasMd')

let listaTareas = [];

async function crear(tarea) {
  listaTareas = cargaDB();
  const nuevaTarea = {
    idTarea: listaTareas.length + 1,
    tarea, // tarea: tarea,
    completado: false
  }
  listaTareas.push(nuevaTarea);
  try {
    return await guardar(listaTareas)
  } catch (error) {
    throw `Error en Base de datos ${error}`
  }
}

function listar() {
  listaTareas = cargaDB();
  listaTareas.forEach(e => {
    console.log('--------------------------------------------------');
    console.log(`ID: ${e.idTarea} | Estado: ${e.completado} | Tarea: ${e.tarea}`);
  })

}

async function borrar(idTarea) {
  listaTareas = cargaDB();
  let listaActualizada = listaTareas.filter(tarea => tarea.idTarea !== idTarea);
  try {
    await guardar(listaActualizada)
    return 'Tarea Borrada'
  } catch (error) {
    throw `Error en Base de datos ${error}`
  }
}

async function terminado(idTarea) {
  listaTareas = cargaDB();
  let indice = listaTareas.findIndex(tarea => tarea.idTarea === idTarea);
  if (indice >= 0) {
    listaTareas[indice].completado = true;
    try {
      await guardar(listaTareas)
      return 'Tarea Terminada'
    } catch (error) {
      throw `Error en Base de datos ${error}`
    }
  }
}

function menu(argv) {
  const comando = argv._[0];
  const idtarea = argv.indice;
  switch (comando) {
    case 'crear':
      const tarea = argv.tarea;
      crear(tarea)
        .then(resp => console.log(resp))
        .catch(e => console.error(e));
      break;
    case 'listar':
      listar()
      break;
    case 'borrar':
      borrar(idtarea)
        .then(resp => console.log(resp))
        .catch(e => console.error(e));
      break;
    case 'terminado':
      // const idtarea = argv.indice;
      terminado(idtarea)
        .then(resp => console.log(resp))
        .catch(e => console.error(e));
      break;
    default:
      console.log('Comando desconocido...');
      break;
  }
}

module.exports = {
  menu
}
