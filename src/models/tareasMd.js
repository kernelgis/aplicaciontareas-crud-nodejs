const fs = require('fs');

function guardar(data) {
  return new Promise((resolve, reject) => {
    fs.writeFile('./src/db/tareas.json', JSON.stringify(data), (err) => {
      if(err) reject(err);
      else 
        resolve('Tarea Registrada')
    } )
  })
}

function cargaDB() {
  const data = require('../db/tareas.json');
  return data;
}

module.exports = {
  guardar, cargaDB
}
